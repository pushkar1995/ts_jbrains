//TS Classes

class Person {
    firstName : string;
    lastName : string;
}

var aPerson = new Person();
aPerson.firstName = "Koushik";
console.log(aPerson);

var aPerson : Person  = new Person();  //declaring the variable as class type for explicit typing
aPerson.firstName = "Koushik";
console.log(aPerson);

var aPerson = new Person();  //declaring the variable as class type for implicit typing
aPerson.firstName = "Koushik";
console.log(aPerson);