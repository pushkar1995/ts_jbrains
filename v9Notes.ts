//V:Typing With Functions

//Typing function arguments

//Optional and default arguments

//Specifying function return type

function add(a : number,b : number) {
    return a + b;
}

var sum = add(1,2);
console.log(sum);

// var sum = add("foo",2); 
//to prevent developer to assign any type.Declare types for the parameters like (a : number,b : number)
// console.log(sum);