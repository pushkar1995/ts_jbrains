// Implicit Typing with variable declarations
// Implicit typing with function calls

var a = 10;
var b = true;
var c = "Hello";

//a = true; //incorrect statement

function greet() : string {
    return "Good morning";
}

var greeting;
greeting = greet();
greeting = 10;


