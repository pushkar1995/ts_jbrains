var fn = () => 'response';

//Both Same

var fn = function() {
    return 'response';
}

var a: number; //declearing the type for the variable in typeScript.

a = 10;
//a = true; //is not possible with ts.