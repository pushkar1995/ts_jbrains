var a: number;
var b: boolean;
var c: string;

a = 10;
b = true;
c = 'Hello';

// var myArr : number[];
// myArr = [];
// myArr = [1,2];
// myArr.push(1);
// a = myArr.pop();

//b = myArr.pop(); //cant do this because its a boolean


// tuple

//For array declaration, you specify the data type before
// the []. For tuple declarations, the data types are specified like
//elements inside the [].

var myArr : [number, boolean];

myArr = [1, true];
myArr = [100, false];
//myArr = [1]; //cant do this [statement not correct]
